# API to compute and validate Italian Italian Tax code (codice fiscale).

Based on the library https://www.npmjs.com/package/codice-fiscale-js

(with both arch versions: arm & x86)

### How does it work


For a valid Italian 'Fiscal Code' this POST to `http://domain/validate`will return `true`

```
$  curl -X POST -H "Content-Type: application/json" -d '{"name":"ciccio","surname":"formaggio","gender":"maschio","date_of_birth":"22/05/1970","fiscal_code":"FRMCCC70E22F205J"}' https://fiscalcode-qa.boat.opencontent.io/validate
true

```