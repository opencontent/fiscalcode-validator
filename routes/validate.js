const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const validator = require('codice-fiscale-js');
const jsonResponse = require('../modules/jsonResponse');
const logger = require('../modules/logger');
const moment = require('moment');



router.use(bodyParser.json());

router.route('/')
  .post(function(request, response) {
    logger.info(`POST ${request.originalUrl} with data ${JSON.stringify(request.body)}`);

    let requestCf = request.body.fiscal_code;
    if (requestCf.length !== 16) {
      return jsonResponse(response, "Codice fiscale non valido", 400);
    }
    try {
      let date_of_birth = moment(request.body.date_of_birth, 'DD/MM/YYYY')
      let user = {
        name: request.body.name,
        surname: request.body.surname,
        gender: request.body.gender.charAt(0).toUpperCase(),
        day: date_of_birth.format('DD'),
        month: date_of_birth.format('MM'),
        year: date_of_birth.format('YYYY'),
        birthplace: requestCf.substring(11, 15),
      }
      let calculated_fiscal_code = validator.compute(user);
      if (calculated_fiscal_code === requestCf) {
        jsonResponse(response, true, 200)
      } else {
        jsonResponse(response, false, 406)
      }
    } catch (error) {
      logger.info(error)
      // Controllo omocodie
      if (validator.check(requestCf)) {
        jsonResponse(response, true, 200);
      } else {
        jsonResponse(response, error.message, 400);
      }
    }

  });


module.exports = router;

