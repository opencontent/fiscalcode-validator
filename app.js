const express = require('express');
const bodyParser = require('body-parser');
const logger = require('./modules/logger');

require('dotenv').config();

const cors = require('cors');

const app = express();
app.use(express.json())


app.use(bodyParser.json({limit: '10mb', extended: true}));
app.use(bodyParser.urlencoded({limit: '10mb', extended: true}));

// use it before all route definitions
app.use(cors());

// APIs
const validateRouter = require('./routes/validate');


app.get('/favicon.ico', (req, res) => res.status(204));
app.use('/validate', validateRouter);


app.set('port', (process.env.PORT || 8000));

app.listen(app.get('port'), function () {
  logger.info(`Globo app is running on port ${app.get('port')}`);
});

module.exports = app; // for testing
