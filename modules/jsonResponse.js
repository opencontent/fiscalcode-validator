module.exports = function(res, data, statusCode) {
  statusCode = parseInt(statusCode, 10);
  console.log(statusCode, data)

  // res.header('cache-control', `public, max-age: ${process.env.MAX_AGE || 0}, s-max-age: ${process.env.S_MAX_AGE || 60}`);
  //Enabling CORS
  res.header('Access-Control-Allow-Origin', '*');
  //Support header x-access-token for the authentication token
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, x-access-token, Authorization');
  res.header('Content-Type', 'application/json');

  if (statusCode === 200) {
    res.status(statusCode).json(data);
  } else {
    res.status(statusCode).send({
      error: data
    });
  }
};
